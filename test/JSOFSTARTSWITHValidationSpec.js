import {expect} from "chai";
import {CONSTANTS, JSOF} from "../src/index";
import testData from "./data/TestData";

describe("STARTSWITH Operator Validation", function () {
    var testResults = {
        STARTSWITH: {
            STRING: {
                CONDITION: {
                    key: "city",
                    condition: CONSTANTS.OPERATORS.STARTSWITH,
                    value: "San"
                },
                RESULTS: [{
                    name: 'Test User',
                    addressLine1: '3200 Bonkers Lane',
                    addressLine2: 'Apt 456',
                    city: 'San Francisco',
                    state: 'CA',
                    zip: 12345,
                    isAdult: true
                }, {
                    name: 'Test User 2',
                    addressLine1: '2300 Crazy Street',
                    addressLine2: '# 564',
                    city: 'San Jose',
                    state: 'CA',
                    zip: 95134,
                    isAdult: false
                }, {
                    name: 'Test User 3',
                    addressLine1: '5564 Hack Ave',
                    addressLine2: '',
                    city: 'San Diego',
                    state: 'CA',
                    zip: 55664,
                    isAdult: undefined
                }, {
                    name: 'Test User 4',
                    addressLine1: '5564 Hack Ave',
                    addressLine2: '',
                    city: 'San Diego',
                    state: 'CA',
                    zip: 55664,
                    isAdult: true
                }]
            },
            NUMBER: {
                CONDITION: {
                    key: "zip",
                    condition: CONSTANTS.OPERATORS.STARTSWITH,
                    value: 54612
                },
                RESULTS: [{
                    name: "Test User 5",
                    addressLine1: "Mindy Cindy",
                    addressLine2: "",
                    city: "New York",
                    state: "NY",
                    zip: 54612,
                    isAdult: true
                }, {
                    name: "Test User 6",
                    addressLine1: "Mindy Cindy",
                    addressLine2: "",
                    city: "New York",
                    state: "NY",
                    zip: 54612,
                    isAdult: true
                }, {
                    name: "Test User 6",
                    addressLine1: "Crazy Mindy Cindy",
                    addressLine2: "#666",
                    city: "New York",
                    state: "NY",
                    zip: 54612,
                    isAdult: true
                }]
            },
            BOOLEAN: {
                CONDITION: {
                    key: "isAdult",
                    condition: CONSTANTS.OPERATORS.STARTSWITH,
                    value: false
                },
                RESULTS: [{
                    name: 'Test User 2',
                    addressLine1: '2300 Crazy Street',
                    addressLine2: '# 564',
                    city: 'San Jose',
                    state: 'CA',
                    zip: 95134,
                    isAdult: false
                }, {
                    name: 'Test User 7',
                    addressLine1: 'Thinking Out Loud Street',
                    addressLine2: 'Apt 434',
                    city: 'Seattle',
                    state: 'WA',
                    zip: 97454,
                    isAdult: false
                }, {
                    name: 'Test User 8',
                    addressLine1: 'That Other Place',
                    addressLine2: 'Apt 123',
                    city: 'Bothell',
                    state: 'WA',
                    zip: 51311,
                    isAdult: false
                }]
            },
            MIXED_AND: {
                CONDITION: {
                    key: "zip",
                    condition: CONSTANTS.OPERATORS.STARTSWITH,
                    value: 54612,
                    and: {
                        key: "addressLine1",
                        condition: CONSTANTS.OPERATORS.STARTSWITH,
                        value: "Mindy",
                        and: {
                            key: "name",
                            condition: CONSTANTS.OPERATORS.STARTSWITH,
                            value: "Test User 5"
                        }
                    }
                },
                RESULTS: [{
                    name: 'Test User 5',
                    addressLine1: 'Mindy Cindy',
                    addressLine2: '',
                    city: 'New York',
                    state: 'NY',
                    zip: 54612,
                    isAdult: true
                }]
            },
            MIXED_OR: {
                CONDITION: {
                    key: "zip",
                    condition: CONSTANTS.OPERATORS.STARTSWITH,
                    value: 54612,
                    or: {
                        key: "addressLine1",
                        condition: CONSTANTS.OPERATORS.STARTSWITH,
                        value: "Crazy",
                        or: {
                            key: "name",
                            condition: CONSTANTS.OPERATORS.STARTSWITH,
                            value: "Test User 5"
                        }
                    }
                },
                RESULTS: [{
                    name: 'Test User 5',
                    addressLine1: 'Mindy Cindy',
                    addressLine2: '',
                    city: 'New York',
                    state: 'NY',
                    zip: 54612,
                    isAdult: true
                }, {
                    name: 'Test User 6',
                    addressLine1: 'Crazy Mindy Cindy',
                    addressLine2: '#666',
                    city: 'New York',
                    state: 'NY',
                    zip: 54612,
                    isAdult: true
                }, {
                    name: 'Test User 6',
                    addressLine1: 'Mindy Cindy',
                    addressLine2: '',
                    city: 'New York',
                    state: 'NY',
                    zip: 54612,
                    isAdult: true
                }]
            }
        }
    };

    it("Operator: STARTSWITH, Type: String, Condition Type: N/A, Condition: " + JSON.stringify(testResults.STARTSWITH.STRING.CONDITION), function () {
        var returnValue = JSOF(testData, testResults.STARTSWITH.STRING.CONDITION);
        expect(returnValue).to.eql(testResults.STARTSWITH.STRING.RESULTS);
    });

    it("Operator: STARTSWITH, Type: Number, Condition Type: N/A, Condition: " + JSON.stringify(testResults.STARTSWITH.NUMBER.CONDITION), function () {
        var returnValue = JSOF(testData, testResults.STARTSWITH.NUMBER.CONDITION);
        expect(returnValue).to.eql(testResults.STARTSWITH.NUMBER.RESULTS);
    });

    it("Operator: STARTSWITH, Type: Boolean, Condition Type: N/A, Condition: " + JSON.stringify(testResults.STARTSWITH.BOOLEAN.CONDITION), function () {
        var returnValue = JSOF(testData, testResults.STARTSWITH.BOOLEAN.CONDITION);
        expect(returnValue).to.eql(testResults.STARTSWITH.BOOLEAN.RESULTS);
    });

    it("Operator: STARTSWITH, Type: Mixed, Condition Type: AND, Condition: " + JSON.stringify(testResults.STARTSWITH.MIXED_AND.CONDITION), function () {
        var returnValue = JSOF(testData, testResults.STARTSWITH.MIXED_AND.CONDITION);
        expect(returnValue).to.eql(testResults.STARTSWITH.MIXED_AND.RESULTS);
    });

    it("Operator: STARTSWITH, Type: Mixed, Condition Type: OR, Condition: " + JSON.stringify(testResults.STARTSWITH.MIXED_OR.CONDITION), function () {
        var returnValue = JSOF(testData, testResults.STARTSWITH.MIXED_OR.CONDITION);
        expect(returnValue).to.eql(testResults.STARTSWITH.MIXED_OR.RESULTS);
    });
});