import {expect} from "chai";
import {CONSTANTS, JSOF} from "../src/index";
import testData from "./data/TestData";

describe("EQUALS Operator Validation", function () {
    var testResults = {
        EQUALS: {
            STRING: {
                CONDITION: {
                    key: "city",
                    condition: CONSTANTS.OPERATORS.EQUALS,
                    value: "San Francisco"
                },
                RESULTS: [{
                    name: 'Test User',
                    addressLine1: '3200 Bonkers Lane',
                    addressLine2: 'Apt 456',
                    city: 'San Francisco',
                    state: 'CA',
                    zip: 12345,
                    isAdult: true
                }]
            },
            NUMBER: {
                CONDITION: {
                    key: "zip",
                    condition: CONSTANTS.OPERATORS.EQUALS,
                    value: 54612
                },
                RESULTS: [{
                    name: "Test User 5",
                    addressLine1: "Mindy Cindy",
                    addressLine2: "",
                    city: "New York",
                    state: "NY",
                    zip: 54612,
                    isAdult: true
                }, {
                    name: "Test User 6",
                    addressLine1: "Mindy Cindy",
                    addressLine2: "",
                    city: "New York",
                    state: "NY",
                    zip: 54612,
                    isAdult: true
                }, {
                    name: "Test User 6",
                    addressLine1: "Crazy Mindy Cindy",
                    addressLine2: "#666",
                    city: "New York",
                    state: "NY",
                    zip: 54612,
                    isAdult: true
                }]
            },
            BOOLEAN: {
                CONDITION: {
                    key: "isAdult",
                    condition: CONSTANTS.OPERATORS.EQUALS,
                    value: false
                },
                RESULTS: [{
                    name: 'Test User 2',
                    addressLine1: '2300 Crazy Street',
                    addressLine2: '# 564',
                    city: 'San Jose',
                    state: 'CA',
                    zip: 95134,
                    isAdult: false
                }, {
                    name: 'Test User 7',
                    addressLine1: 'Thinking Out Loud Street',
                    addressLine2: 'Apt 434',
                    city: 'Seattle',
                    state: 'WA',
                    zip: 97454,
                    isAdult: false
                }, {
                    name: 'Test User 8',
                    addressLine1: 'That Other Place',
                    addressLine2: 'Apt 123',
                    city: 'Bothell',
                    state: 'WA',
                    zip: 51311,
                    isAdult: false
                }]
            },
            MIXED_AND: {
                CONDITION: {
                    key: "zip",
                    condition: CONSTANTS.OPERATORS.EQUALS,
                    value: 54612,
                    and: {
                        key: "addressLine1",
                        condition: CONSTANTS.OPERATORS.EQUALS,
                        value: "Mindy Cindy",
                        and: {
                            key: "name",
                            condition: CONSTANTS.OPERATORS.EQUALS,
                            value: "Test User 5"
                        }
                    }
                },
                RESULTS: [{
                    name: 'Test User 5',
                    addressLine1: 'Mindy Cindy',
                    addressLine2: '',
                    city: 'New York',
                    state: 'NY',
                    zip: 54612,
                    isAdult: true
                }]
            },
            MIXED_OR: {
                CONDITION: {
                    key: "zip",
                    condition: CONSTANTS.OPERATORS.EQUALS,
                    value: 54612,
                    or: {
                        key: "addressLine1",
                        condition: CONSTANTS.OPERATORS.EQUALS,
                        value: "Mindy Cindy",
                        or: {
                            key: "name",
                            condition: CONSTANTS.OPERATORS.EQUALS,
                            value: "Test User 5"
                        }
                    }
                },
                RESULTS: [{
                    name: 'Test User 5',
                    addressLine1: 'Mindy Cindy',
                    addressLine2: '',
                    city: 'New York',
                    state: 'NY',
                    zip: 54612,
                    isAdult: true
                }, {
                    name: 'Test User 6',
                    addressLine1: 'Mindy Cindy',
                    addressLine2: '',
                    city: 'New York',
                    state: 'NY',
                    zip: 54612,
                    isAdult: true
                }, {
                    name: 'Test User 6',
                    addressLine1: 'Crazy Mindy Cindy',
                    addressLine2: '#666',
                    city: 'New York',
                    state: 'NY',
                    zip: 54612,
                    isAdult: true
                }]
            },
            MIXED_AND_OR: {
                CONDITION: {
                    key: "zip",
                    condition: CONSTANTS.OPERATORS.EQUALS,
                    value: 54612,
                    and: {
                        key: "name",
                        condition: CONSTANTS.OPERATORS.EQUALS,
                        value: "Test User 5",
                        and: {
                            key: "InvalidKey",
                            condition: CONSTANTS.OPERATORS.EQUALS,
                            value: "InvalidKey"
                        },
                        or: {
                            key: "addressLine1",
                            condition: CONSTANTS.OPERATORS.EQUALS,
                            value: "Mindy Cindy"
                        }
                    }
                },
                RESULTS: [{
                    name: 'Test User 5',
                    addressLine1: 'Mindy Cindy',
                    addressLine2: '',
                    city: 'New York',
                    state: 'NY',
                    zip: 54612,
                    isAdult: true
                }, {
                    name: 'Test User 6',
                    addressLine1: 'Mindy Cindy',
                    addressLine2: '',
                    city: 'New York',
                    state: 'NY',
                    zip: 54612,
                    isAdult: true
                }]
            }
        }
    };

    it("Operator: EQUALS, Type: String, Condition Type: N/A, Condition: " + JSON.stringify(testResults.EQUALS.STRING.CONDITION), function () {
        var returnValue = JSOF(testData, testResults.EQUALS.STRING.CONDITION);
        expect(returnValue).to.eql(testResults.EQUALS.STRING.RESULTS);
    });

    it("Operator: EQUALS, Type: Number, Condition Type: N/A, Condition: " + JSON.stringify(testResults.EQUALS.NUMBER.CONDITION), function () {
        var returnValue = JSOF(testData, testResults.EQUALS.NUMBER.CONDITION);
        expect(returnValue).to.eql(testResults.EQUALS.NUMBER.RESULTS);
    });

    it("Operator: EQUALS, Type: Boolean, Condition Type: N/A, Condition: " + JSON.stringify(testResults.EQUALS.BOOLEAN.CONDITION), function () {
        var returnValue = JSOF(testData, testResults.EQUALS.BOOLEAN.CONDITION);
        expect(returnValue).to.eql(testResults.EQUALS.BOOLEAN.RESULTS);
    });

    it("Operator: EQUALS, Type: Mixed, Condition Type: AND, Condition: " + JSON.stringify(testResults.EQUALS.MIXED_AND.CONDITION), function () {
        var returnValue = JSOF(testData, testResults.EQUALS.MIXED_AND.CONDITION);
        expect(returnValue).to.eql(testResults.EQUALS.MIXED_AND.RESULTS);
    });

    it("Operator: EQUALS, Type: Mixed, Condition Type: OR, Condition: " + JSON.stringify(testResults.EQUALS.MIXED_OR.CONDITION), function () {
        var returnValue = JSOF(testData, testResults.EQUALS.MIXED_OR.CONDITION);
        expect(returnValue).to.eql(testResults.EQUALS.MIXED_OR.RESULTS);
    });

    it("Operator: EQUALS, Type: Mixed, Condition Type: AND & OR, Condition: " + JSON.stringify(testResults.EQUALS.MIXED_OR.CONDITION), function () {
        var returnValue = JSOF(testData, testResults.EQUALS.MIXED_AND_OR.CONDITION);
        expect(returnValue).to.eql(testResults.EQUALS.MIXED_AND_OR.RESULTS);
    });
});