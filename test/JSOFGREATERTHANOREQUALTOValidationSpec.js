import {expect} from "chai";
import {CONSTANTS, JSOF} from "../src/index";
import testData from "./data/TestData";

describe("GREATERTHAN Operator Validation", function () {
    var testResults = {
        GREATERTHAN: {
            STRING: {
                CONDITION: {
                    key: "city",
                    condition: CONSTANTS.OPERATORS.GREATERTHAN,
                    value: "San"
                },
                RESULTS: [{
                    name: 'Test User',
                    addressLine1: '3200 Bonkers Lane',
                    addressLine2: 'Apt 456',
                    city: 'San Francisco',
                    state: 'CA',
                    zip: 12345,
                    isAdult: true
                }, {
                    name: 'Test User 2',
                    addressLine1: '2300 Crazy Street',
                    addressLine2: '# 564',
                    city: 'San Jose',
                    state: 'CA',
                    zip: 95134,
                    isAdult: false
                }, {
                    name: 'Test User 3',
                    addressLine1: '5564 Hack Ave',
                    addressLine2: '',
                    city: 'San Diego',
                    state: 'CA',
                    zip: 55664,
                    isAdult: undefined
                }, {
                    name: 'Test User 4',
                    addressLine1: '5564 Hack Ave',
                    addressLine2: '',
                    city: 'San Diego',
                    state: 'CA',
                    zip: 55664,
                    isAdult: true
                }, {
                    name: 'Test User 7',
                    addressLine1: 'Thinking Out Loud Street',
                    addressLine2: 'Apt 434',
                    city: 'Seattle',
                    state: 'WA',
                    zip: 97454,
                    isAdult: false
                }]
            },
            NUMBER: {
                CONDITION: {
                    key: "zip",
                    condition: CONSTANTS.OPERATORS.GREATERTHAN,
                    value: 54612
                },
                RESULTS: [{
                    name: 'Test User 2',
                    addressLine1: '2300 Crazy Street',
                    addressLine2: '# 564',
                    city: 'San Jose',
                    state: 'CA',
                    zip: 95134,
                    isAdult: false
                }, {
                    name: 'Test User 3',
                    addressLine1: '5564 Hack Ave',
                    addressLine2: '',
                    city: 'San Diego',
                    state: 'CA',
                    zip: 55664,
                    isAdult: undefined
                }, {
                    name: 'Test User 4',
                    addressLine1: '5564 Hack Ave',
                    addressLine2: '',
                    city: 'San Diego',
                    state: 'CA',
                    zip: 55664,
                    isAdult: true
                }, {
                    name: 'Test User 7',
                    addressLine1: 'Thinking Out Loud Street',
                    addressLine2: 'Apt 434',
                    city: 'Seattle',
                    state: 'WA',
                    zip: 97454,
                    isAdult: false
                }]
            },
            BOOLEAN: {
                CONDITION: {
                    key: "isAdult",
                    condition: CONSTANTS.OPERATORS.GREATERTHAN,
                    value: false
                },
                RESULTS: [{
                    name: 'Test User',
                    addressLine1: '3200 Bonkers Lane',
                    addressLine2: 'Apt 456',
                    city: 'San Francisco',
                    state: 'CA',
                    zip: 12345,
                    isAdult: true
                }, {
                    name: 'Test User 4',
                    addressLine1: '5564 Hack Ave',
                    addressLine2: '',
                    city: 'San Diego',
                    state: 'CA',
                    zip: 55664,
                    isAdult: true
                }, {
                    name: 'Test User 5',
                    addressLine1: 'Mindy Cindy',
                    addressLine2: '',
                    city: 'New York',
                    state: 'NY',
                    zip: 54612,
                    isAdult: true
                }, {
                    name: 'Test User 6',
                    addressLine1: 'Mindy Cindy',
                    addressLine2: '',
                    city: 'New York',
                    state: 'NY',
                    zip: 54612,
                    isAdult: true
                }, {
                    name: 'Test User 6',
                    addressLine1: 'Crazy Mindy Cindy',
                    addressLine2: '#666',
                    city: 'New York',
                    state: 'NY',
                    zip: 54612,
                    isAdult: true
                }]
            },
            MIXED_AND: {
                CONDITION: {
                    key: "zip",
                    condition: CONSTANTS.OPERATORS.GREATERTHAN,
                    value: 54612,
                    and: {
                        key: "addressLine1",
                        condition: CONSTANTS.OPERATORS.GREATERTHAN,
                        value: "Mindy",
                        and: {
                            key: "name",
                            condition: CONSTANTS.OPERATORS.GREATERTHAN,
                            value: "Test User 5"
                        }
                    }
                },
                RESULTS: [{
                    name: 'Test User 7',
                    addressLine1: 'Thinking Out Loud Street',
                    addressLine2: 'Apt 434',
                    city: 'Seattle',
                    state: 'WA',
                    zip: 97454,
                    isAdult: false
                }]
            },
            MIXED_OR: {
                CONDITION: {
                    key: "zip",
                    condition: CONSTANTS.OPERATORS.GREATERTHAN,
                    value: 54612,
                    or: {
                        key: "addressLine1",
                        condition: CONSTANTS.OPERATORS.GREATERTHAN,
                        value: "Crazy",
                        or: {
                            key: "name",
                            condition: CONSTANTS.OPERATORS.GREATERTHAN,
                            value: "Test User 5"
                        }
                    }
                },
                RESULTS: [{
                    name: 'Test User 6',
                    addressLine1: 'Mindy Cindy',
                    addressLine2: '',
                    city: 'New York',
                    state: 'NY',
                    zip: 54612,
                    isAdult: true
                }, {
                    name: 'Test User 6',
                    addressLine1: 'Crazy Mindy Cindy',
                    addressLine2: '#666',
                    city: 'New York',
                    state: 'NY',
                    zip: 54612,
                    isAdult: true
                }, {
                    name: 'Test User 7',
                    addressLine1: 'Thinking Out Loud Street',
                    addressLine2: 'Apt 434',
                    city: 'Seattle',
                    state: 'WA',
                    zip: 97454,
                    isAdult: false
                }, {
                    name: 'Test User 8',
                    addressLine1: 'That Other Place',
                    addressLine2: 'Apt 123',
                    city: 'Bothell',
                    state: 'WA',
                    zip: 51311,
                    isAdult: false
                }, {
                    name: 'Test User 9',
                    addressLine1: 'Next To That Other Place',
                    addressLine2: 'Apt 456',
                    city: 'Bothell',
                    state: 'WA',
                    zip: 51311,
                    isAdult: null
                }, {
                    name: 'Test User 5',
                    addressLine1: 'Mindy Cindy',
                    addressLine2: '',
                    city: 'New York',
                    state: 'NY',
                    zip: 54612,
                    isAdult: true
                }, {
                    name: 'Test User 2',
                    addressLine1: '2300 Crazy Street',
                    addressLine2: '# 564',
                    city: 'San Jose',
                    state: 'CA',
                    zip: 95134,
                    isAdult: false
                }, {
                    name: 'Test User 3',
                    addressLine1: '5564 Hack Ave',
                    addressLine2: '',
                    city: 'San Diego',
                    state: 'CA',
                    zip: 55664,
                    isAdult: undefined
                }, {
                    name: 'Test User 4',
                    addressLine1: '5564 Hack Ave',
                    addressLine2: '',
                    city: 'San Diego',
                    state: 'CA',
                    zip: 55664,
                    isAdult: true
                }]
            }
        }
    };

    it("Operator: GREATERTHAN, Type: String, Condition Type: N/A, Condition: " + JSON.stringify(testResults.GREATERTHAN.STRING.CONDITION), function () {
        var returnValue = JSOF(testData, testResults.GREATERTHAN.STRING.CONDITION);
        expect(returnValue).to.eql(testResults.GREATERTHAN.STRING.RESULTS);
    });

    it("Operator: GREATERTHAN, Type: Number, Condition Type: N/A, Condition: " + JSON.stringify(testResults.GREATERTHAN.NUMBER.CONDITION), function () {
        var returnValue = JSOF(testData, testResults.GREATERTHAN.NUMBER.CONDITION);
        expect(returnValue).to.eql(testResults.GREATERTHAN.NUMBER.RESULTS);
    });

    it("Operator: GREATERTHAN, Type: Boolean, Condition Type: N/A, Condition: " + JSON.stringify(testResults.GREATERTHAN.BOOLEAN.CONDITION), function () {
        var returnValue = JSOF(testData, testResults.GREATERTHAN.BOOLEAN.CONDITION);
        expect(returnValue).to.eql(testResults.GREATERTHAN.BOOLEAN.RESULTS);
    });

    it("Operator: GREATERTHAN, Type: Mixed, Condition Type: AND, Condition: " + JSON.stringify(testResults.GREATERTHAN.MIXED_AND.CONDITION), function () {
        var returnValue = JSOF(testData, testResults.GREATERTHAN.MIXED_AND.CONDITION);
        expect(returnValue).to.eql(testResults.GREATERTHAN.MIXED_AND.RESULTS);
    });

    it("Operator: GREATERTHAN, Type: Mixed, Condition Type: OR, Condition: " + JSON.stringify(testResults.GREATERTHAN.MIXED_OR.CONDITION), function () {
        var returnValue = JSOF(testData, testResults.GREATERTHAN.MIXED_OR.CONDITION);
        expect(returnValue).to.eql(testResults.GREATERTHAN.MIXED_OR.RESULTS);
    });
});