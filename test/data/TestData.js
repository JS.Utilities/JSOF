const testData = [{
    name: "Test User",
    addressLine1: "3200 Bonkers Lane",
    addressLine2: "Apt 456",
    city: "San Francisco",
    state: "CA",
    zip: 12345,
    isAdult: true
}, {
    name: "Test User 2",
    addressLine1: "2300 Crazy Street",
    addressLine2: "# 564",
    city: "San Jose",
    state: "CA",
    zip: 95134,
    isAdult: false
}, {
    name: "Test User 3",
    addressLine1: "5564 Hack Ave",
    addressLine2: "",
    city: "San Diego",
    state: "CA",
    zip: 55664,
    isAdult: undefined
}, {
    name: "Test User 4",
    addressLine1: "5564 Hack Ave",
    addressLine2: "",
    city: "San Diego",
    state: "CA",
    zip: 55664,
    isAdult: true
}, {
    name: "Test User 5",
    addressLine1: "Mindy Cindy",
    addressLine2: "",
    city: "New York",
    state: "NY",
    zip: 54612,
    isAdult: true
}, {
    name: "Test User 6",
    addressLine1: "Mindy Cindy",
    addressLine2: "",
    city: "New York",
    state: "NY",
    zip: 54612,
    isAdult: true
}, {
    name: "Test User 6",
    addressLine1: "Crazy Mindy Cindy",
    addressLine2: "#666",
    city: "New York",
    state: "NY",
    zip: 54612,
    isAdult: true
}, {
    name: "Test User 7",
    addressLine1: "Thinking Out Loud Street",
    addressLine2: "Apt 434",
    city: "Seattle",
    state: "WA",
    zip: 97454,
    isAdult: false
}, {
    name: "Test User 8",
    addressLine1: "That Other Place",
    addressLine2: "Apt 123",
    city: "Bothell",
    state: "WA",
    zip: 51311,
    isAdult: false
}, {
    name: "Test User 9",
    addressLine1: "Next To That Other Place",
    addressLine2: "Apt 456",
    city: "Bothell",
    state: "WA",
    zip: 51311,
    isAdult: null
}, {
    name: "Test User 10",
    addressLine1: "",
    addressLine2: "",
    city: "",
    state: "",
    zip: undefined,
    isAdult: null
}];

export default testData;