import {expect} from "chai";
import {CONSTANTS, JSOF} from "../src/index";
import testData from "./data/TestData";

describe("CONTAINS Operator Validation -", function () {
    var testResults = {
        CONTAINS: {
            STRING: {
                CONDITION: {
                    key: "city",
                    condition: CONSTANTS.OPERATORS.CONTAINS,
                    value: "San Francisco"
                },
                RESULTS: [{
                    name: 'Test User',
                    addressLine1: '3200 Bonkers Lane',
                    addressLine2: 'Apt 456',
                    city: 'San Francisco',
                    state: 'CA',
                    zip: 12345,
                    isAdult: true
                }]
            },
            NUMBER: {
                CONDITION: {
                    key: "zip",
                    condition: CONSTANTS.OPERATORS.CONTAINS,
                    value: 54612
                },
                RESULTS: [{
                    name: "Test User 5",
                    addressLine1: "Mindy Cindy",
                    addressLine2: "",
                    city: "New York",
                    state: "NY",
                    zip: 54612,
                    isAdult: true
                }, {
                    name: "Test User 6",
                    addressLine1: "Mindy Cindy",
                    addressLine2: "",
                    city: "New York",
                    state: "NY",
                    zip: 54612,
                    isAdult: true
                }, {
                    name: "Test User 6",
                    addressLine1: "Crazy Mindy Cindy",
                    addressLine2: "#666",
                    city: "New York",
                    state: "NY",
                    zip: 54612,
                    isAdult: true
                }]
            },
            BOOLEAN: {
                CONDITION: {
                    key: "isAdult",
                    condition: CONSTANTS.OPERATORS.CONTAINS,
                    value: false
                },
                RESULTS: [{
                    name: 'Test User 2',
                    addressLine1: '2300 Crazy Street',
                    addressLine2: '# 564',
                    city: 'San Jose',
                    state: 'CA',
                    zip: 95134,
                    isAdult: false
                }, {
                    name: 'Test User 7',
                    addressLine1: 'Thinking Out Loud Street',
                    addressLine2: 'Apt 434',
                    city: 'Seattle',
                    state: 'WA',
                    zip: 97454,
                    isAdult: false
                }, {
                    name: 'Test User 8',
                    addressLine1: 'That Other Place',
                    addressLine2: 'Apt 123',
                    city: 'Bothell',
                    state: 'WA',
                    zip: 51311,
                    isAdult: false
                }]
            },
            MIXED_AND: {
                CONDITION: {
                    key: "zip",
                    condition: CONSTANTS.OPERATORS.CONTAINS,
                    value: 54612,
                    and: {
                        key: "addressLine1",
                        condition: CONSTANTS.OPERATORS.CONTAINS,
                        value: "Mindy Cindy",
                        and: {
                            key: "name",
                            condition: CONSTANTS.OPERATORS.CONTAINS,
                            value: "Test User 5"
                        }
                    }
                },
                RESULTS: [{
                    name: 'Test User 5',
                    addressLine1: 'Mindy Cindy',
                    addressLine2: '',
                    city: 'New York',
                    state: 'NY',
                    zip: 54612,
                    isAdult: true
                }]
            },
            MIXED_OR: {
                CONDITION: {
                    key: "zip",
                    condition: CONSTANTS.OPERATORS.CONTAINS,
                    value: 54612,
                    or: {
                        key: "addressLine1",
                        condition: CONSTANTS.OPERATORS.CONTAINS,
                        value: "Mindy Cindy",
                        or: {
                            key: "name",
                            condition: CONSTANTS.OPERATORS.CONTAINS,
                            value: "Test User 5"
                        }
                    }
                },
                RESULTS: [{
                    name: 'Test User 5',
                    addressLine1: 'Mindy Cindy',
                    addressLine2: '',
                    city: 'New York',
                    state: 'NY',
                    zip: 54612,
                    isAdult: true
                }, {
                    name: 'Test User 6',
                    addressLine1: 'Mindy Cindy',
                    addressLine2: '',
                    city: 'New York',
                    state: 'NY',
                    zip: 54612,
                    isAdult: true
                }, {
                    name: 'Test User 6',
                    addressLine1: 'Crazy Mindy Cindy',
                    addressLine2: '#666',
                    city: 'New York',
                    state: 'NY',
                    zip: 54612,
                    isAdult: true
                }]
            }
        }
    };

    it("Operator: CONTAINS, Type: String, Condition Type: N/A, Condition: " + JSON.stringify(testResults.CONTAINS.STRING.CONDITION), function () {
        var returnValue = JSOF(testData, testResults.CONTAINS.STRING.CONDITION);
        expect(returnValue).to.eql(testResults.CONTAINS.STRING.RESULTS);
    });

    it("Operator: CONTAINS, Type: Number, Condition Type: N/A, Condition: " + JSON.stringify(testResults.CONTAINS.NUMBER.CONDITION), function () {
        var returnValue = JSOF(testData, testResults.CONTAINS.NUMBER.CONDITION);
        expect(returnValue).to.eql(testResults.CONTAINS.NUMBER.RESULTS);
    });

    it("Operator: CONTAINS, Type: Boolean, Condition Type: N/A, Condition: " + JSON.stringify(testResults.CONTAINS.BOOLEAN.CONDITION), function () {
        var returnValue = JSOF(testData, testResults.CONTAINS.BOOLEAN.CONDITION);
        expect(returnValue).to.eql(testResults.CONTAINS.BOOLEAN.RESULTS);
    });

    it("Operator: CONTAINS, Type: Mixed, Condition Type: AND, Condition: " + JSON.stringify(testResults.CONTAINS.MIXED_AND.CONDITION), function () {
        var returnValue = JSOF(testData, testResults.CONTAINS.MIXED_AND.CONDITION);
        expect(returnValue).to.eql(testResults.CONTAINS.MIXED_AND.RESULTS);
    });

    it("Operator: CONTAINS, Type: Mixed, Condition Type: OR, Condition: " + JSON.stringify(testResults.CONTAINS.MIXED_OR.CONDITION), function () {
        var returnValue = JSOF(testData, testResults.CONTAINS.MIXED_OR.CONDITION);
        expect(returnValue).to.eql(testResults.CONTAINS.MIXED_OR.RESULTS);
    });
});