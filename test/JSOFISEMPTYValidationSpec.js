import {expect} from "chai";
import {CONSTANTS, JSOF} from "../src/index";
import testData from "./data/TestData";

describe("IS EMPTY Operator Validation", function () {
    var testResults = {
        ISEMPTY: {
            STRING: {
                CONDITION: {
                    key: "city",
                    condition: CONSTANTS.OPERATORS.ISEMPTY
                },
                RESULTS: [{
                    name: 'Test User 10',
                    addressLine1: '',
                    addressLine2: '',
                    city: '',
                    state: '',
                    zip: undefined,
                    isAdult: null
                }]
            },
            NUMBER: {
                CONDITION: {
                    key: "zip",
                    condition: CONSTANTS.OPERATORS.ISEMPTY
                },
                RESULTS: [{
                    name: 'Test User 10',
                    addressLine1: '',
                    addressLine2: '',
                    city: '',
                    state: '',
                    zip: undefined,
                    isAdult: null
                }]
            },
            BOOLEAN: {
                CONDITION: {
                    key: "isAdult",
                    condition: CONSTANTS.OPERATORS.ISEMPTY
                },
                RESULTS: [{
                    name: 'Test User 3',
                    addressLine1: '5564 Hack Ave',
                    addressLine2: '',
                    city: 'San Diego',
                    state: 'CA',
                    zip: 55664,
                    isAdult: undefined
                }, {
                    name: 'Test User 9',
                    addressLine1: 'Next To That Other Place',
                    addressLine2: 'Apt 456',
                    city: 'Bothell',
                    state: 'WA',
                    zip: 51311,
                    isAdult: null
                }, {
                    name: 'Test User 10',
                    addressLine1: '',
                    addressLine2: '',
                    city: '',
                    state: '',
                    zip: undefined,
                    isAdult: null
                }]
            },
            MIXED_AND: {
                CONDITION: {
                    key: "zip",
                    condition: CONSTANTS.OPERATORS.ISEMPTY,
                    and: {
                        key: "isAdult",
                        condition: CONSTANTS.OPERATORS.ISEMPTY,
                        and: {
                            key: "name",
                            condition: CONSTANTS.OPERATORS.ISEMPTY
                        }
                    }
                },
                RESULTS: []
            },
            MIXED_OR: {
                CONDITION: {
                    key: "zip",
                    condition: CONSTANTS.OPERATORS.ISEMPTY,
                    or: {
                        key: "addressLine1",
                        condition: CONSTANTS.OPERATORS.ISEMPTY,
                        or: {
                            key: "name",
                            condition: CONSTANTS.OPERATORS.ISEMPTY
                        }
                    }
                },
                RESULTS: [{
                    name: 'Test User 10',
                    addressLine1: '',
                    addressLine2: '',
                    city: '',
                    state: '',
                    zip: undefined,
                    isAdult: null
                }]
            }
        }
    };

    it("Operator: ISEMPTY, Type: String, Condition Type: N/A, Condition: " + JSON.stringify(testResults.ISEMPTY.STRING.CONDITION), function () {
        var returnValue = JSOF(testData, testResults.ISEMPTY.STRING.CONDITION);
        expect(returnValue).to.eql(testResults.ISEMPTY.STRING.RESULTS);
    });

    it("Operator: ISEMPTY, Type: Number, Condition Type: N/A, Condition: " + JSON.stringify(testResults.ISEMPTY.NUMBER.CONDITION), function () {
        var returnValue = JSOF(testData, testResults.ISEMPTY.NUMBER.CONDITION);
        expect(returnValue).to.eql(testResults.ISEMPTY.NUMBER.RESULTS);
    });

    it("Operator: ISEMPTY, Type: Boolean, Condition Type: N/A, Condition: " + JSON.stringify(testResults.ISEMPTY.BOOLEAN.CONDITION), function () {
        var returnValue = JSOF(testData, testResults.ISEMPTY.BOOLEAN.CONDITION);
        expect(returnValue).to.eql(testResults.ISEMPTY.BOOLEAN.RESULTS);
    });

    it("Operator: ISEMPTY, Type: Mixed, Condition Type: AND, Condition: " + JSON.stringify(testResults.ISEMPTY.MIXED_AND.CONDITION), function () {
        var returnValue = JSOF(testData, testResults.ISEMPTY.MIXED_AND.CONDITION);
        expect(returnValue).to.eql(testResults.ISEMPTY.MIXED_AND.RESULTS);
    });

    it("Operator: ISEMPTY, Type: Mixed, Condition Type: OR, Condition: " + JSON.stringify(testResults.ISEMPTY.MIXED_OR.CONDITION), function () {
        var returnValue = JSOF(testData, testResults.ISEMPTY.MIXED_OR.CONDITION);
        expect(returnValue).to.eql(testResults.ISEMPTY.MIXED_OR.RESULTS);
    });
});