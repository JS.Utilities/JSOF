import {expect} from "chai";
import {CONSTANTS, JSOF} from "../src/index";

describe("Input Validation -", function () {

    it("Condition: Input, Condition Type: Invalid number of input arguments", function () {
        var returnFunction = function () {
            JSOF([]);
        }
        expect(returnFunction).to.throw(Error, "Invalid input data set or condtion set type. Please see usage.");
    });

    it("Condition: Input, Condition Type: Empty & Valid input parameters and Invalid Condition parameter", function () {
        var returnFunction = function () {
            JSOF([], {});
        }
        expect(returnFunction).to.throw(Error, "Invalid values for condition set. Please see usage.");
    });

    it("Condition: Input, Condition Type: Invalid data set type", function () {
        var returnFunction = function () {
            JSOF({}, {});
        }
        expect(returnFunction).to.throw(Error, "Invalid input data set or condtion set type. Please see usage.");
    })

    it("Condition: Input, Condition Type: Valid data set type", function () {
        var returnValue = JSOF([], {
            key: "key",
            condition: CONSTANTS.OPERATORS.EQUALS,
            value: "value"
        });
        expect(returnValue).to.eql([]);
    })
});