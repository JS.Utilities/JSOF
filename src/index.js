/**
 * Description: A utility to filter objects based on conditional attributes.
 *   The method 'filter' accepts the data set, conditional operators and the
 *   operation type and returns the final result
 * Author: Gautam Bhutani (buildthedesign@gmail.com)
 **/

// Operator constants
const CONSTANTS = {
    OPERATORS: {
        EQUALS: 1,
        NOTEQUALS: 2,
        CONTAINS: 3,
        NOTCONTAINS: 4,
        ISEMPTY: 5,
        ISNOTEMPTY: 6,
        LESSTHAN: 7,
        GREATERTHAN: 8,
        STARTSWITH: 9,
        ENDSWITH: 10,
        LESSTHANOREQUALTO: 11,
        GREATERTHANOREQUALTO: 12
    }
};

/*
    @description: Filter the incoming data set based on the conditions and operation
    @param: dataSetArray Array containing the data set
    @param: conditonObject Array containing the condition set
    @return: filtered array of objects
    @throw: throw exception on errors
*/
function JSOF(dataSetArray, conditonObject) {
    "use strict";

    function _filter(dataSetArray, conditonObject) {
        let returnArray = dataSetArray; //Return of this function defaults to the data set
        let returnCollection = {}; //Inernal object collection to store object hashes
        let orArray = []; //Return of an OR operation

        //Validate the data set and the condition object
        _validateInput(dataSetArray, conditonObject);

        //Filter the original data set by the provided condition
        returnArray = returnArray.filter((obj, index) => {
            return _filterCallback(conditonObject, obj, index);
        }, this);

        //If the condition has a nested AND condition, process that
        if (conditonObject.and) {
            //Validate the filtered data set and the nested and
            _validateInput(returnArray, conditonObject.and);

            //Filter the already filtered data set by the provided condition (recursively)
            returnArray = _filter(returnArray, conditonObject.and);
        }

        //If the condition has a nested OR condition, process that
        if (conditonObject.or) {
            //We don't need to validate because the OR condition acts on the original
            //data set and not on the already filtered data set. Only the AND operates
            //on the already filtered data set. This is because the OR operation
            //would result in different results per condition, where as, the AND
            //operation would need to filter again on the filtered results

            //Filter the original data set by the provided condition
            orArray = _filter(dataSetArray, conditonObject.or);
        }

        /* So far we have done the following:
         * 1. Filter the original data set on the first condition
         * 2. Recursively filter the resultant of #1 on the AND condition
         * 3. Filter the original data set on the nested OR condition of the
         *    first condition
         * We have the following arrays:
         * 1. returnArray = First condition on the original data set + nested AND condition
         * 2. orArray = Nested OR condition on the original data set
         *
         * If we do not have an orArray, we are good to go; however, if we do -
         * The two arrays might contain duplicate objects; hence, now we remove duplicates
         * and create the final result array
         */
        if (orArray.length > 0) {
            //Let the orArray contain the results of all operations so far
            orArray = orArray.concat(returnArray);

            //Re-initialize returnArray to empty (we already have it's result
            //concatinated to orArray)
            returnArray = [];

            //Iterate through objects in orArray
            orArray.forEach((element, index, array) => {
                let elementKeys = Object.keys(element).sort(); //Get the sorted attributes (keys) of the object
                let elementHash = ""; //variable to store the hash

                //Iterate through the attributes of the object and create a hash
                for (var elementCount = 0; elementCount < elementKeys.length; elementCount++) {
                    //Get the value of each attribute and create a string hash
                    elementHash = elementHash + element[elementKeys[elementCount]];
                }

                //Check if the hash exists in our internal collection; if it does,
                //ignore it as it's a duplicate of one we have already processed
                if (!returnCollection[elementHash]) {
                    //If the hash does not exist, this is a brand new object that has not been evaluated yet
                    //Add the object to the collection
                    returnCollection[elementHash] = element;

                    //Add the object to the final array
                    returnArray.push(element);
                }
            });
        }
        //Return the final array of objects
        return returnArray;
    }
    return _filter(dataSetArray, conditonObject);
};


/*
    @description: Callback function for the array.filter function
    @param: currentContition current condition
    @param: obj object being operated on
    @param: index index of the object being operated on
    @return: boolean - true/false if the condition is satisfied
*/
function _filterCallback(currentCondition, obj, index) {
    "use strict";

    //A few condtions (equals, contains, starts with, etc) need the attribute to be present;
    //howeve, a few don't because if they are not there, the condition is still satisfied;
    //hence, we set a variable for cases which do not require the attribute to be present
    const conditionValue = currentCondition.value;
    const condition = currentCondition.condition;
    const conditionValuePassThrough = condition === CONSTANTS.OPERATORS.NOTEQUALS || condition === CONSTANTS.OPERATORS.NOTCONTAINS || condition === CONSTANTS.OPERATORS.ISEMPTY;

    if (obj.hasOwnProperty(currentCondition.key) || conditionValuePassThrough) {
        let objectValue = obj[currentCondition.key];
        switch (condition) {
            case CONSTANTS.OPERATORS.EQUALS:
                return objectValue === conditionValue;
            case CONSTANTS.OPERATORS.NOTEQUALS:
                return objectValue !== conditionValue;
            case CONSTANTS.OPERATORS.CONTAINS:
                return String(objectValue).indexOf(conditionValue) !== -1;
            case CONSTANTS.OPERATORS.NOTCONTAINS:
                return String(objectValue).indexOf(conditionValue) === -1;
            case CONSTANTS.OPERATORS.ISEMPTY:
                return objectValue === undefined || objectValue === null || String(objectValue).trim() === "";
            case CONSTANTS.OPERATORS.ISNOTEMPTY:
                return (objectValue !== undefined && objectValue !== null) && String(objectValue).trim() !== "";
            case CONSTANTS.OPERATORS.LESSTHAN:
                return objectValue < conditionValue;
            case CONSTANTS.OPERATORS.GREATERTHAN:
                return objectValue > conditionValue;
            case CONSTANTS.OPERATORS.LESSTHANOREQUALTO:
                return objectValue <= conditionValue;
            case CONSTANTS.OPERATORS.GREATERTHANOREQUALTO:
                return objectValue >= conditionValue;
            case CONSTANTS.OPERATORS.STARTSWITH:
                return String(objectValue).startsWith(conditionValue);
            case CONSTANTS.OPERATORS.ENDSWITH:
                return String(objectValue).endsWith(conditionValue);
        }
    };
    return false;
};

/*
  @description: Check if the condition requires the condition value
  @param: condition condition
  @param: true/false if the condition requires the condition value
*/
function _isConditionValueNeeded(condition) {
    "use strict";
    return !(condition === CONSTANTS.OPERATORS.ISEMPTY || condition === CONSTANTS.OPERATORS.ISNOTEMPTY);
};

/*
  @description: Method to validate inputs for existance, type and value.
  @param: dataSetArray Array containing the data set
  @param: conditonObject Object containing the condition set
  @throw: throw exception on errors
*/
function _validateInput(dataSetArray, conditonObject) {
    "use strict";

    /* Check if the data types is correct */
    if (!Array.isArray(dataSetArray) || Array.isArray(conditonObject) || typeof conditonObject !== "object") {
        throw new Error("Invalid input data set or condtion set type. Please see usage.");
    }

    /* Validate input data set values - it has to be an object only */
    dataSetArray.forEach(function (value, index) {
        if (typeof value !== "object" || Array.isArray(value)) {
            throw new Error("Invalid values for input data set. Please see usage.");
        }
    });

    /* Validate conditional operators - it has to be an object only with valid key, condition and value attributes */
    if (typeof conditonObject !== "object" || Array.isArray(conditonObject) || !conditonObject.condition || !conditonObject.key || (_isConditionValueNeeded.call(this, conditonObject.condition) && (conditonObject.value === undefined || conditonObject.value === null || conditonObject.value === "")) || !_hasOwnValue(CONSTANTS.OPERATORS, conditonObject.condition)) {
        throw new Error("Invalid values for condition set. Please see usage.");
    }
};

/*
    @description: Function to check if an object contains a value or not
    @param element element to be checked
    @param value value to be checked
    @return true/false if the object contains the value
*/
function _hasOwnValue(element, value) {
    "use strict";

    for (var prop in element) {
        if (element.hasOwnProperty(prop) && element[prop] === value) {
            return true;
        }
    }
}

// Export
export {
    CONSTANTS,
    JSOF
};