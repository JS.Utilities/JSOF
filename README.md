JavaScript Object Filter (JSOF)
===============================

A modular JavaScript library that enables the user to filter JavaScript objects on nested conditions.
This is meant to be a micro-library and is supposed to only filter objects and do it well.

It is different from other filtering libraries as it exposes two operations and multiple conditional operators.


| Operations  | Conditional Operators |
| ------------- | ------------- |
| OR  | EQUALS  |
| AND  | NOTEQUALS  |
| Nested OR/AND  | CONTAINS  |
|   | NOTCONTAINS  |
|   | ISEMPTY  |
|   | ISNOTEMPTY  |
|   | LESSTHAN  |
|   | LESSTHANOREQUALTO  |
|   | GREATERTHAN  |
|   | GREATERTHANOREQUALTO  |
|   | STARTSWITH  |
|   | ENDSWITH  |


Each of these operators can be applied to any of the objects attributes.

Attribute value arrays are objects are not supported.

Contract & Usage
========
- API Call:
```js
import {CONSTANTS, JSOF} from "@btd/jsof";

JSOF( <[Object Data Array]>, <{Condition Object}>, );
```

- Data Set = Array of objects - [{ Object 1}, { Object 2}, ..., { Object n}]

  Object attribute values have to be String, Number or Boolean.

  Arrays or nested objects as attribute values is not supported.

- Condition = Object with the following contract:
```js
{
  key: "<attribute name>",
  condition: <operator>,
  value: <value to be tested>,
  and: <optional - condition object>,
  or: <optional - condition object>
}
```

Example
=======
The below conditions can be easily interpreted and satisfied by the library -
- A & B
- A || B
- (A & B) || C
- (A & B) || (C & D)
- etc.

Example:
```js
import {CONSTANTS, JSOF} from "@btd/jsof";

var dataSet = [
    { name: "Mark", age: 22, city: "New York", isLoggedIn: false},
    { name: "Cindy", age: 18, city: "San Francisco", isLoggedIn: true},
    { name: "Linda", age: 25, city: "Washington", isLoggedIn: true},
    { name: "Jack", age: 30, city: "Seattle", isLoggedIn: false},
  ];

// A & B
JSOF(dataSet, {
  key: "isLoggedIn",
  condition: CONSTANTS.OPERATORS.EQUALS,
  value: true,
  and: {
    key: "age",
    condition: CONSTANTS.OPERATORS.GREATERTHAN,
    value: 20
  }
});
//Result: [{"name":"Linda","age":25,"city":"Washington","isLoggedIn":true}]

// A || B
JSOF(dataSet, {
  key: "isLoggedIn",
  condition: CONSTANTS.OPERATORS.EQUALS,
  value: false,
  or: {
    key: "age",
    condition: CONSTANTS.OPERATORS.GREATERTHAN,
    value: 20
  }
});
//Result: [{"name":"Mark","age":22,"city":"New York","isLoggedIn":false},{"name":"Linda","age":25,"city":"Washington","isLoggedIn":true},{"name":"Jack","age":30,"city":"Seattle","isLoggedIn":false}]


// (A || B) & C === C & (A || B)
JSOF(dataSet, {
  key: "isLoggedIn",
  condition: CONSTANTS.OPERATORS.EQUALS,
  value: false,
  and: {
    key: "city",
    condition: CONSTANTS.OPERATORS.EQUALS,
    value: "Seattle",
    or: {
      key: "age",
      condition: CONSTANTS.OPERATORS.GREATERTHAN,
      value: 20
    }
  }
});
//Result: [{"name":"Mark","age":22,"city":"New York","isLoggedIn":false},{"name":"Jack","age":30,"city":"Seattle","isLoggedIn":false}]
```


Problem Statement
=================
http://blog.gautambhutani.com/javascript-object-filter-problem-statement/

Design & Contract
==================
http://blog.gautambhutani.com/javascript-object-filter-design/

Development
===========
http://blog.gautambhutani.com/javascript-object-filter-nested-filter/
